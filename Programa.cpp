#include <iostream>
#include "Hash.h"

using namespace std;

/*FUNCIONES PARA LA EJECUCION DEL PROGRAMA*/

/*Funcion Menu*/
int menu(){
  string opcion;
  cout << "\n--------Menu Principal--------" << endl;
  cout << "|[1] Insertar numeros al arreglo|" << endl;
  cout << "|[2] Buscar numero en arreglo   |" << endl;
  cout << "|[3] Salir                      |" << endl;
  cout << "------------------------------" << endl;
  cout << "\nIngrese Opcion: ";
  getline(cin, opcion);

  /*Validacion si la opcion es correcta*/
  while ((stoi(opcion) > 3) || (stoi(opcion) < 1)){
    cout << "\nOPCION NO VALIDA\nIngrese Opcion:";
    getline(cin, opcion);
  }

  return stoi(opcion);
}

/*Funcion para llenar un arreglo vacio*/
void rellenarArreglo(int arreglo[], int N){
  for(int i=0; i<N; i++){
    arreglo[i] = '\0';
  }
}

/*Funcion para imprimir arreglo*/
void imprimir(int arreglo[], int N){
  cout << "--------------------------------------------------------------\n";
  cout << "Arreglo\n";
  cout << "--------------------------------------------------------------\n";
  for(int i=0; i<N; i++){
    if(arreglo[i] == 0){
      cout << "[]-";
    }
    else{
      cout << "[" << arreglo[i] << "]-";
    }
  }
  cout << "\n--------------------------------------------------------------\n";
  cout << endl;
}

/*Funcion para conocer si esta o no vacio el arreglo*/
bool arregloVacio(int arreglo[], int N){
  int c=0;

  for(int i=0; i<N; i++){
    if(arreglo[i] == '\0'){
      c++;
    }
  }
  if(c == N){
    return true;
  }
  else{
    return false;
  }
}

/*Funcion que decide el metodo de solucion de colisiones al ejecutar, en caso
  de insertar un numero*/
void ejecutarPrueba_Insercion(int arreglo[], int N, int k, string colision){
  Hash *hash = new Hash();

  /*pruebaLineal*/
  if( (colision.compare("L") == 0) || (colision.compare("l") == 0) ){
    hash->pruebaLineal_Insertar(arreglo, N, k);
  }

  /*pruebaCuadratica*/
  if( (colision.compare("C") == 0) || (colision.compare("c") == 0) ){
    hash->pruebaCuadratica_Insertar(arreglo, N, k);
  }

  /*pruebaDobleDireccion*/
  if( (colision.compare("D") == 0)||(colision.compare("d") == 0) ){
    hash->dobleDireccion_Insertar(arreglo, N, k);
  }

  /*pruebaEncadenamiento*/
  if( (colision.compare("E") == 0) || (colision.compare("e") == 0) ) {
    hash->encadenamiento(arreglo, N, k);
  }

}

/*Funcion que decide el metodo de solucion de colisiones al ejecutar, en caso
  de buscar un numero*/
void ejecutarPrueba_busqueda(int arreglo[], int N, int k, string colision){
  Hash *hash = new Hash();

  /*pruebaLineal*/
  if ( (colision.compare("L") == 0) || (colision.compare("l") == 0) ){
    hash->pruebaLineal(arreglo, N, k);
  }

  /*pruebaCuadratica*/
  if((colision.compare("C") == 0) || (colision.compare("c") == 0)){
    hash->pruebaCuadratica(arreglo, N, k);
  }

  /*pruebaDobleDireccion*/
  if((colision.compare("D") == 0)||(colision.compare("d") == 0)){
    hash->dobleDireccion(arreglo, N, k);
  }

  /*pruebaEncadenamiento*/
  if(colision.compare("E") == 0 || (colision.compare("e") == 0)){
    hash->encadenamiento(arreglo, N, k);
  }

}

/*----------------------------------------------------------------------------------*/

/*PROGRAMA*/
int main( int argc, char *argv[] ) {

  system("clear");
  /*parametro para el metodo de colision*/
  string colision;
  if(argc == 2){
    colision = argv[1];
  }
  else{
    cout << "Se necesita un parametro" << endl;
    return 1;
  }

  /*Variables*/
  int N = 20;
  int opcion = 0;
  string line;
  int num;
  int pos;
  int arreglo[N];
  rellenarArreglo(arreglo, N);
  Hash *hash = new Hash();

  while(opcion != 3){
    opcion = menu();
    system("clear");
    switch (opcion) {
      /*Insertar numero en el arreglo*/
      case 1:
        cout << "Ingrese numero: ";
        getline(cin, line);
        num = stoi(line);
        /*Obtencion de posicion con Hash*/
        pos = hash->hashModulo(num);
        /*Si no existe nada en esa poscion se asigna*/
        if(arreglo[pos] == '\0'){
          arreglo[pos] = num;
          imprimir(arreglo, N);
        }
        /*De lo contrario, existe colision y se debe ejecutar algun metodo*/
        else{
          cout << "Colision en posicion: " << pos+1 << endl;
          ejecutarPrueba_Insercion(arreglo, N, num, colision);
          imprimir(arreglo, N);
        }
        break;

      /*Busqueda de elementos en el arreglo*/
      case 2:
        /*Si el arreglo no esta vacio, entonces se puede buscar*/
        if(arregloVacio(arreglo, N) == false){
          cout << "Ingrese numero: ";
          getline(cin, line);
          num = stoi(line);
          /*Obtencion de posicion con hash*/
          pos = hash->hashModulo(num);
          if(arreglo[pos] == num){
            cout << "La clave se encuentra en la posicion: " << pos+1 << endl;
            imprimir(arreglo, N);
          }
          else{
            ejecutarPrueba_busqueda(arreglo, N, num, colision);
            imprimir(arreglo, N);
          }
        }
        else{
          cout << "Arreglo Vacio \n";
        }
        break;
      /*Salir*/
      case 3:
        break;
    }
  }
}
