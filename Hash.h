#include <iostream>
#include <stdlib.h>

using namespace std;

//Estructura para nodo
typedef struct _Nodo{
	int numero;
	struct _Nodo *sig;
} Nodo;

#ifndef HASH_H
#define HASH_H

class Hash{
	public:
		/*Constructor*/
		Hash(){}

    /*Metodos*/
    /*Funciones Hash*/
    int hashModulo(int clave);
    int hashAux(int clave);

    /*Metodo que implementa una prueba lineal de colisiones usado
      para insertar los elementos al arreglo*/
    void pruebaLineal_Insertar(int arreglo[], int N, int k);

    /*Metodo que implementa una prueba lineal de colisiones para
      buscar un elemento en el arreglo*/
    void pruebaLineal(int arreglo[], int N, int k);

    /*Metodo que implementa una prueba cuadratica de colisiones para
      insertar los elementos al arreglo*/
    void pruebaCuadratica_Insertar(int arreglo[], int N, int k);

    /*Metodo que implementa una prueba cuadratica de colisiones para
      buscar un elemento en el arreglo*/
    void pruebaCuadratica(int arreglo[], int N, int k);

    /*Metodo que implementa una prueba de doble direccion para
      insertar un elemento al arreglo*/
    void dobleDireccion_Insertar(int arreglo[], int N, int k);

    /*Metodo que implementa una prueba de doble direccion para
      buscar un elemente en el arreglo*/
    void dobleDireccion(int arreglo[], int N, int k);

    /*Metodo que copia arreglo en el arreglo indicado en metodo de encadenamiento*/
    void copiarArreglo(Nodo *arreglo1[], int arreglo2[], int N);

    /*Metodo para imprimir una lista*/
    void imprimirLista(Nodo *arreglo[], int D);

    /*Metodo de colisiones por encadenamiento*/
    void encadenamiento(int arreglo[], int N, int k);
};
#endif
