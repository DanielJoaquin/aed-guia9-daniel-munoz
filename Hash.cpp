#include <iostream>
#include "Hash.h"

using namespace std;

/*Metodos*/
/*Funciones Hash*/
int Hash::hashModulo(int clave){
    int pos;
    pos = (clave%19)+1;
    return pos;
}

int Hash::hashAux(int clave){
  int pos;
  pos = (clave+1)%20+1;
  return pos;
}

/*Metodo que implementa una prueba lineal de colisiones usado
  para insertar los elementos al arreglo*/
void Hash::pruebaLineal_Insertar(int arreglo[], int N, int k){
  int D;
  int DX;
  int c;

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada en la posicion: " << D+1 << endl;
  }
  else{
    DX = D+1;
    while((DX <= N) && (arreglo[DX] != '\0') && (arreglo[DX] != k) && (DX != D)){
      DX = DX + 1;
      if(DX == (N + 1)){
        DX = 1;
      }
      c++;
    }
    if(arreglo[DX] == '\0'){
      arreglo[DX] = k;
      cout << "La clave se movio a la posicion: " << DX + 1 << endl;
    }
    if(c == N){
      cout << "El arreglo esta lleno\n";
    }
  }
}

/*Metodo que implementa una prueba lineal de colisiones para
  buscar un elemento en el arreglo*/
void Hash::pruebaLineal(int arreglo[], int N, int k){
  int D;
  int DX;

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada en la posicion: " << D+1 << endl;
  }
  else{
    DX = D+1;
    while((DX <= N) && (arreglo[DX] != '\0') && (arreglo[DX] != k) && (DX != D)){
      DX = DX + 1;
      if(DX == (N + 1)){
        DX = 1;
      }
    }
    if((arreglo[DX] == '\0') || (DX == D)){
      cout << "La clave no se encuentra en el arreglo\n";
    }
    else{
      cout << "La clave esta en la posicion: " << DX + 1 << endl;
    }
  }
}

/*Metodo que implementa una prueba cuadratica de colisiones para
  insertar los elementos al arreglo*/
void Hash::pruebaCuadratica_Insertar(int arreglo[], int N, int k){
  int D;
  int DX;
  int i;

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada en la posicion: " << D+1 << endl;
  }
  else{
    i=0;
    DX = D+(i*i);
    while((arreglo[DX] != '\0') && (arreglo[DX] != k)){
      i++;
      DX = DX + (i*i);
      if(DX > N){
        i = 0;
        DX = 1;
        D = 1;
      }
    }
    if(arreglo[DX] == '\0'){
      arreglo[DX] = k;
      cout << "La clave se movio a la posicion: " << DX +1<< endl;
    }
    else{
      cout << "Clave encontrada en posicion: " << DX << endl;
    }
  }

}

/*Metodo que implementa una prueba cuadratica de colisiones para
  buscar un elemento en el arreglo*/
void Hash::pruebaCuadratica(int arreglo[], int N, int k){
  int D;
  int DX;
  int i;

  D = hashModulo(k);
  cout << D << endl;
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada en la posicion: " << D << endl;
  }
  else{
    i=1;
    DX = D+(i*i);
    while((arreglo[DX] != '\0') && (arreglo[DX] != k)){
      i++;
      DX = DX + (i*i);
      if(DX > N){
        i = 0;
        DX = 1;
        D = 1;
      }
    }
    if(arreglo[DX] == '\0'){
      cout << "La clave no se encuentra en el arreglo\n";
    }
    else{
      cout << "Clave encontrada en posicion: " << DX +1<< endl;
    }
  }
}

/*Metodo que implementa una prueba de doble direccion para
  insertar un elemento al arreglo*/
void Hash::dobleDireccion_Insertar(int arreglo[], int N, int k){
  int D;
  int DX;

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada esta en la posicion: " << D+1 << endl;
  }
  else{
    DX = hashAux(D);
    while((DX <= N) && (arreglo[DX] != '\0') && (arreglo[DX] != k) && (DX != D)){
      DX = hashAux(DX);
    }
    if((arreglo[DX] == '\0') || (arreglo[DX] != k)){
      arreglo[DX] = k;
      if(DX == 21){
        cout << "El arreglo esta lleno \n";
      }
      else{
        cout << "La clave se movio a la posicion: " << DX +1<< endl;
      }
    }
    else{
      cout << "La clave encontrada esta en la posicion: " << DX << endl;
    }
  }

}

/*Metodo que implementa una prueba de doble direccion para
  buscar un elemente en el arreglo*/
void Hash::dobleDireccion(int arreglo[], int N, int k){
  int D;
  int DX;

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "Clave encontrada en la posicion: " << D+1 << endl;
  }
  else{
    DX = hashAux(D);
    while((DX <= N) && (arreglo[DX] != '\0') && (arreglo[DX] != k) && (DX != D)){
      DX = hashAux(DX);
    }
    if((arreglo[DX] == '\0') || (DX == D)){
      cout << "La clave no se encuentra en el arreglo \n";

    }
    else{
      cout << "La clave encontrada esta en la posicion: " << DX << endl;
    }
  }
}

/*Metodo que copia arreglo en el arreglo indicado en metodo de encadenamiento*/
void Hash::copiarArreglo(Nodo *arreglo1[], int arreglo2[], int N){
  for(int i=0; i<N; i++){
    arreglo1[i]->numero = arreglo2[i];
    arreglo1[i]->sig = NULL;
  }
}

/*Metodo para imprimir una lista*/
void Hash::imprimirLista(Nodo *arreglo[], int D){
  Nodo *aux = NULL;
  aux = arreglo[D];
  while(aux != NULL){
    if(aux->numero != 0){
      cout << "[" << aux->numero << "]-" << endl;
    }
    aux = aux->sig;
  }
}

/*Metodo de colisiones por encadenamiento*/
void Hash::encadenamiento(int arreglo[], int N, int k){
  int D;
  Nodo *q = NULL;
  Nodo *V[N];

  for(int i=0; i<N; i++){
    V[i] = new Nodo();
    V[i]->sig = NULL;
    V[i]->numero = '\0';
  }
  copiarArreglo(V, arreglo, N);

  D = hashModulo(k);
  if((arreglo[D] != '\0') && (arreglo[D] == k)){
    cout << "La clave encontrada esta en la posicion: " << D+1 << endl;
  }
  else{
    q = V[D]->sig;
    while((q != NULL) && (q->numero != k)){
      q = q->sig;
    }
    if(q == NULL){
      cout << "La clave no se encuentra en la lista" << endl;
    }
    else{
      cout << "La clave se encuentra en la posicion: " << D+1 << endl;
    }
  }
  imprimirLista(V, D);
}
